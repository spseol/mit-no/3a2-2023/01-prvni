#include "stm8s.h"
#include "main.h"
#include "milis.h"
#include "daughterboard.h"
#include "delay.h"
//#include <stdio.h>
//#include "uart1.h"


void setup(void)
{

    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);      // taktovani MCU na 16MHz

    GPIO_Init(LED6_PORT, LED6_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(LED7_PORT, LED7_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(LED8_PORT, LED8_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    
    // RGB LED
    GPIO_Init(PWMR_PORT, PWMR_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(PWMG_PORT, PWMG_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(PWMB_PORT, PWMB_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    
    GPIO_Init(S1_PORT, S1_PIN, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(S2_PORT, S2_PIN, GPIO_MODE_IN_PU_NO_IT);
    GPIO_Init(S3_PORT, S3_PIN, GPIO_MODE_IN_PU_NO_IT);

    init_milis();
    //init_uart1();
}


int main(void)
{
  
    uint32_t time6 = 0;
    uint32_t time7 = 0;
    uint32_t time8 = 0;
    uint8_t btn_push = 0;   // pamatuje si, že tlačítko bylo stisknuté
    uint32_t time_btn = 0;
    uint32_t time_led = 0;

    uint16_t counter = 0;
    

    setup();

    delay_ms(1234);

    while (1) {

        if (milis() - time_btn > 20) {
            time_btn = milis();
            
            if (PUSH(S1)) {
                btn_push = 1;
                counter++;
            } else {
                if (btn_push == 1) { // tady jsem to tlacito pustil
                    HIGH(PWMR);
                }
                btn_push = 0;
            }
        }

        if ( (milis() - time_led) > 100) {
            time_led = milis();

            if (READ(PWMR) == 1) {
                if (counter > 0) {
                    counter--;
                } else {
                    LOW(PWMR);
                }
            }
        }

        if (milis() - time6 > 777 ) {
            time6 = milis();
            REVERSE(LED6);
        }
        if (milis() - time7 > 333 ) {
            time7 = milis();
            REVERSE(LED7);
        }
        if (milis() - time8 > 2000 ) {
            time8 = milis();
            REVERSE(LED8);
        }
    }
}

/*-------------------------------  Assert -----------------------------------*/
#include "__assert__.h"
